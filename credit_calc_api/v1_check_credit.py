from fastapi import APIRouter, HTTPException, Request

from calc.calc import check_credit_permit
from dto.v1.credit_model import CreditData

router = APIRouter()


@router.post('/check_credit_available', description='Check credit')
async def check_credit(request: CreditData):
    if request.age < 0:
        raise HTTPException(status_code=404, detail="age field is wrong")
    elif request.gender not in ['F', 'M']:
        raise HTTPException(status_code=404, detail="gender field is wrong")
    elif request.source_income not in ['пассивный доход', 'наёмный работник', 'собственный бизнес', 'безработный']:
        raise HTTPException(status_code=404, detail="source_income field is wrong")
    elif type(request.income_last_year) is not int or request.income_last_year <= 0:
        raise HTTPException(status_code=404, detail="income_last_year field is wrong")
    elif request.credit_rate not in [-2, -1, 0, 1, 2]:
        raise HTTPException(status_code=404, detail="credit_rate field is wrong")
    elif request.credit_sum < 0.1:
        raise HTTPException(status_code=404, detail="credit_sum field is wrong")
    elif request.maturity < 1 or request.maturity > 20:
        raise HTTPException(status_code=404, detail="maturity field is wrong")
    elif request.goal not in ['ипотека', 'развитие бизнеса', 'автокредит', 'потребительский']:
        raise HTTPException(status_code=404, detail="goal field is wrong")
    else:
        return check_credit_permit(request)
