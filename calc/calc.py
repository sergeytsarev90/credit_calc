from math import log


def year_pay(credit_data):
    pay_sum = credit_data.credit_sum * (1 + credit_data.maturity *
                                        (percent_counter(credit_data.goal, credit_data.credit_rate,
                                                         credit_data.credit_sum, credit_data.source_income))
                                        ) / credit_data.maturity
    return pay_sum


def percent_counter(credit_type, rating, credit_sum, income_type):
    base_percent = 10.0
    base_percent += type_percent(credit_type)
    base_percent += rating_percent(rating)
    base_percent += income_type_percent(income_type)

    base_percent += -log(credit_sum)

    return base_percent / 100


def income_type_percent(income_type):
    if income_type == 'пассивный доход':
        return 0.5
    if income_type == 'наёмный работник':
        return -0.25
    if income_type == 'собственный бизнес':
        return 0.25


def rating_percent(rating):
    if rating == -1:
        return 1.5
    elif rating == 1:
        return -0.25
    elif rating == 2:
        return -0.75
    else:
        return 0


def type_percent(credit_type):
    if credit_type == 'ипотека':
        return -2
    elif credit_type == 'развитие бизнеса':
        return -0.5
    elif credit_type == 'потребительский':
        return 1.5
    else:
        return 0


def check_credit_permit(data):
    if (data.age >= 65 and data.gender == 'M') or \
            (data.age >= 60 and data.gender == 'F') or \
            (data.credit_sum / data.maturity > data.income_last_year) or \
            (data.credit_rate == -2) or \
            (data.source_income == 'безработный') or \
            (year_pay(data) > data.income_last_year) or \
            (data.credit_sum > 1 and (data.source_income == 'пассивный доход' or data.credit_rate == -1)) or \
            (data.credit_sum > 5 and (data.source_income == 'наёмный работник' or data.credit_rate == 0)) or \
            (data.credit_sum > 10 and (data.source_income == 'собственный бизнес' or data.credit_rate == 2)):
        return dict(result='Кредит не одобрен')
    else:
        return dict(result='Кредит одобрен', sum=year_pay(data))
